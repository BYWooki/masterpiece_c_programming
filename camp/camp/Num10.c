#include <stdio.h>
int main() {
	int time, second, minute, hour;

	printf("초를 입력하시오 : ");
	scanf_s("%d", &time);

	hour = time / 3600;
	minute = (time % 3600) / 60;
	second = (time % 3600) % 60;
	
	printf("%d초는 %d시간 %d분 %d초입니다.\n", time, hour, minute, second);

	return 0;
}