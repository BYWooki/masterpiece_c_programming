#include <stdio.h>
int main() {
	double a, b;

	printf("첫 번째 소수를 입력하시오 : ");
	scanf_s("%lf", &a);
	printf("두 번째 소수를 입력하시오 : ");
	scanf_s("%lf", &b);

	printf("두 수의 합은 %lf\n", a + b);
	printf("두 수의 차는 %lf\n", a - b);
	printf("두 수의 곱은 %lf\n", a * b);
	printf("두 수의 몫은 %lf\n", a / b);

	return 0;
}