#include <stdio.h>
int main() {
	char string1[100], string2[100], str1[100], str2[100];

	printf("첫 번째 문자열을 입력하세요 : ");
	scanf_s("%s", &string1, sizeof(string1));
	printf("두 번째 문자열을 입력하세요 : ");
	scanf_s("%s", &string2, sizeof(string2));
	
	for (int i = 0; 1; i++) {
		
		if ((string1[i] >= 65) && (string1[i] <= 90)) {
			str1[i] = string1[i] + 32;
		}
		else if ((string1[i] >= 97) && (string1[i] <= 122)) {
			str1[i] = string1[i];
		}

		if ((string2[i] >= 65) && (string2[i] <= 90)) {
			string2[i] = string2[i] + 32;
		}
		else if ((string2[i] >= 97) && (string2[i] <= 122)) {
			str2[i] = string2[i];
		}

		if (str1[i] > str2[i]) {
			printf("%s이(가) %s보다 더 큰 문자열입니다.\n", string1, string2);
			break;
		}
		else if (str1[i] < str2[i]) {
			printf("%s이(가) %s보다 더 큰 문자열입니다.\n", string2, string1);
			break;
		}
		else if (str2[i] == str2[i]) {
			continue;
		}

	} /*두 단어의 길이가 달라도 문자열의 맨 뒤에 위치한 \0의 아스키코드는 0이기에 
	  어떠한 알파벳보다도 작아 신경쓰지 않고 코드를 짜면 된다.*/

	return 0;
}