#include <stdio.h>

int LtoU_UtoL(char letter[]);

int main() {
	char string[100];

	printf("영문 문자열을 입력 : ");
	scanf_s("%s", &string, sizeof(string));

	LtoU_UtoL(string);

	return 0;
}

int LtoU_UtoL(char letter[]) {

	for (int i = 0; letter[i] != '\0'; i++) {
		if ((letter[i] >= 97) && (letter[i] <= 122)) {
			printf("%c", letter[i] - 32);
		}
		else if ((letter[i] >= 65) && (letter[i] <= 90)) {
			printf("%c", letter[i] + 32);
		}
	}

	printf("\n");

	return 0;
}