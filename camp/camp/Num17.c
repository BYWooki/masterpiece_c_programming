#include <stdio.h>
#include <string.h>
int main() {
	char string[100];
	int a, i, j;
	
	printf("문자열 입력 : ");
	gets_s(string, sizeof(string));

	for (i = 0; string[i] != ' ' ; i++) {
		continue;
	}

	a = strlen(string) - 1;

	printf("공백 이후의 나머지 문자열 : ");

	j = i + 1;

	for (j; j <= a; j++) {
		printf("%c", string[j]);
	}
	
	printf("\n");
	printf("첫번째 공백의 위치 : %d\n", i + 1);
	
	return 0;
}