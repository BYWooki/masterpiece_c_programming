#include <stdio.h>
double triangle(double length, double height);

int main() {
	double length, height, result;

	printf("삼각형의 밑변의 길이를 입력하시오 : ");
	scanf_s("%lf", &length, 8);
	printf("삼각형의 높이를 입력하시오 : ");
	scanf_s("%lf", &height, 8);

	result = triangle(length, height);
	printf("밑변의 길이가 %.2lfcm, 높이가 %.2lfcm인 삼각형의 넓이는 %.2lfcm²이다.\n", length, height, result);

	return 0;
}

double triangle(double length, double height) {
	double result;

	result = length * height / 2;

	return result;
}