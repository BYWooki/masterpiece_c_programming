#include <stdio.h>
int main() {
	int a = 1;
	char b = 'a';
	float c = 0.1;
	double d = 0.3;

	printf("int 자료형의 크기는 %d,\nchar 자료형의 크기는 %d,\nfloat 자료형의 크기는 %d,\ndouble 자료형의 크기는 %d이다.\n", sizeof(a), sizeof(b), sizeof(c), sizeof(d));
	
	return 0;
}