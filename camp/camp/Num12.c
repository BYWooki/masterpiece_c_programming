#include <stdio.h>
int main() {
	int integer, a, b, c, d, sum;
	
	printf("4자리 정수를 입력하시오 : ");
	scanf_s("%d", &integer);

	a = integer % 10;
	b = (integer / 10) % 10;
	c = (integer / 100) % 10;
	d = (integer / 1000) % 10;
	sum = a + b + c + d;

	printf("%d + %d + %d + %d = %d\n", d, c, b, a, sum);

	return 0;
}