#include <stdio.h>
#include <string.h>
int main() {
	int a, b, i, j;
	char str1[] = "한성대학교", str2[] = "컴퓨터공학과", str3[100];

	strcpy_s(str3, sizeof(str3), str1);
	printf("(1)str3=%s\n", str3);

	a = strlen(str3);
	b = strlen(str2);

	for (i = a, j = 0; i <= (a + b - 1), j <= b - 1; i++, j++) {
		str3[i] = str2[j];
	}
	
	printf("(2)str3=");

	for (int k = 0; k <= (a + b - 1); k++) {
		printf("%c", str3[k]);
	}

	printf("\n");
	
	return 0;
}