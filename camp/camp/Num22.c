#include <stdio.h>
#include <string.h>
#include <math.h>

int binaryTodecimal(void);

int main() {

	binaryTodecimal();

	return 0;
}

int binaryTodecimal(void) {
	char num[100];
	int length = strlen(num) - 1, sum = 0;
	
	printf("이진수를 입력 : ");
	scanf_s("%s", &num, sizeof(num));

	for (length; length >= 0; length--) {
		if (num[length] == '0') {
			continue;
		}
		else if (num[length] == '1') {
			sum += pow(2, (strlen(num) - 1) - length);
		}
	}

	printf("십진수로 변환하면 %d\n", sum);

	return 0;
}