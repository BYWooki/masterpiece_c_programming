#include <stdio.h>
int main() {
	int price, w50000, w10000, w5000, w1000, c500, c100, c50, c10, c5, c1;
	
	printf("금액을 입력 : ");
	scanf_s("%d", &price);

	w50000 = price / 50000;
	w10000 = (price - (w50000 * 50000)) / 10000;
	w5000 = (price - (price / 10000) * 10000) / 5000;
	w1000 = (price - (price / 5000) * 5000) / 1000;
	c500 = (price - (price / 1000) * 1000) / 500;
	c100 = (price - (price / 500) * 500) / 100;
	c50 = (price - (price / 100) * 100) / 50;
	c10 = (price - (price / 50) * 50) / 10;
	c5 = (price - (price / 10) * 10) / 5;
	c1 = (price - (price / 5) * 5) / 1;

	printf("5만원: %d\n", w50000);
	printf("1만원: %d\n", w10000);
	printf("5천원: %d\n", w5000);
	printf("1천원: %d\n", w1000);
	printf("500원: %d\n", c500);
	printf("100원: %d\n", c100);
	printf(" 50원: %d\n", c50);
	printf(" 10원: %d\n", c10);
	printf("  5원: %d\n", c5);
	printf("  1원: %d\n", c1);

	return 0;
}