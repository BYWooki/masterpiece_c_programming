#include <stdio.h>
int main() {
	int time, second, minute, hour;

	printf("시간을 입력하시오 : ");
	scanf_s("%d", &hour);
	printf("분을 입력하시오 : ");
	scanf_s("%d", &minute);
	printf("초를 입력하시오 : ");
	scanf_s("%d", &second);
	time = 3600 * hour + 60 * minute + second;
	printf("%d시간 %d분 %d초는 %d초입니다.\n", hour, minute, second, time);

	return 0;
}