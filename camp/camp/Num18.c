#include <stdio.h>
#include <string.h>

int noSpace(char string[]);

int main() {
	char string[500];
	
	printf("문자열을 입력 : ");
	gets_s(string, sizeof(string));

	noSpace(string);

	return 0;

}

int noSpace(char string[]) {
	int length = strlen(string) - 1;
	
	for (int i = 0; i <= length; i++) {
		if (string[i] == ' ') {
			continue;
		}
		else if (string[i] == '\0') {
			break;
		}
		else {
			printf("%c", string[i]);
		}
	}

	printf("\n");

	return 0;
}