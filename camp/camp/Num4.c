#include <stdio.h>
double mile_to_kilo(double mile);

int main() {
	double mile, kilo;

	printf("마일 값을 소수로 입력하시오 : ");
	scanf_s("%lf", &mile, 8);

	kilo = mile_to_kilo(mile);
	printf("킬로미터로 변환하면 %lfkm이다.\n", kilo);
	
	return 0;
}

double mile_to_kilo(double mile) {
	double kilo;

	kilo = mile * 1.60935;

	return kilo;
}