#include <stdio.h>
#include <stdlib.h>

int binary_method(int a);

int main() {
	char letter;

	printf("문자 입력 : ");
	scanf_s("%c", &letter, 1);
	printf("10진수: %d ", letter);
	printf("8진수: %o ", letter);
	printf("16진수: %x ", letter);
	printf("2진수: ");
	binary_method(letter);
	printf("\n");
}

int binary_method(int a) {
	int b;

	b = a / 2;

	if (b == 0) {
		printf("%d", a % 2);
	}
	else {
		binary_method(b);
		printf("%d", a % 2);
	}
	
	return 0;
}