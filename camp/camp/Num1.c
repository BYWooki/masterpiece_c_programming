#include <stdio.h>
int compare_number(int x, int y, int z);

int main() {
	int a, b, c, result;

	printf("첫 번째 정수를 입력하시오 : ");
	scanf_s("%d", &a, 4);
	printf("두 번째 정수를 입력하시오 : ");
	scanf_s("%d", &b, 4);
	printf("세 번째 정수를 입력하시오 : ");
	scanf_s("%d", &c, 4);

	result = compare_number(a, b, c);
	printf("가장 큰 수는 %d 입니다\n", result);

	return 0;
}

int compare_number(int x, int y, int z) {
	if (x > y)
		if (x > z)
			return x;
		else if (x < z)
			return z;
		else
			return x;
	else if (x < y)
		if (y > z)
			return y;
		else if (y < z)
			return z;
		else
			return y;
	else if (x == y)
		if (x > z)
			return x;
		else if (x < z)
			return z;
		else
			return x;
	
	return 0;
}