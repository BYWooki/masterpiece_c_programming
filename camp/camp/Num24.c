#include <stdio.h>
int main() {
	char str[20];
	int i, j, len;

	printf("문자열 입력: ");
	gets_s(str, sizeof(str));
	len = 0;

	for (i = 0; str[i] != '\0'; i++)
		len++;

	printf("문자열 길이 = %d\n", len);

	for (i = len; i >= 0; i--) {
		for (j = 0; j <= i-1; j++) {
			printf("%c", str[j]);
		}
		printf("\n");
	}
	
	return 0;
}